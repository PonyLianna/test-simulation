var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");

//game = ctx.fillRect("#000", 0, 0, 480, 320);
ctx.fillStyle = '#000000';
/* ctx.fillRect( 0, 0, 200, 200); */
ctx.width = "500";
ctx.height = "500";

function randomize(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

function reset() {
    ctx.fillStyle = '#ffffff';
    ctx.clearRect(0, 0, ctx.width, ctx.height);
}


class friend {
    constructor() {
        function coordinates(obj) {
            obj.x = randomize(100);
            obj.y = randomize(100);
        }

        this.height = 5;
        this.width = 5;
        ctx.fillRect(this.x, this.y, this.width, this.height);
        coordinates(this);
        if (this.look(5)){
            coordinates(this);
        }
    }

    look(line) {
        /* let line = ; */
        // console.log( ctx.getImageData(this.x - line, this.y - line, this.width + line * 2, this.height + line * 2).data.includes(255));
        return ctx.getImageData(this.x - line, this.y - line, this.width + line * 2, this.height + line * 2).data.includes(255);
        /*  return ctx.getImageData(this.x, this.y, line, line).data.includes(255) || ctx.getImageData(this.x+this.width, this.y, line, line).data.includes(255) || ctx.getImageData(this.x, this.y, this.width, line).data.includes(255) ||
         ctx.getImageData(this.x, this.y+this.height, line, line).data.includes(255);  */
    }

    grow() {
        this.clear();
        if (this.look(5)) {
            this.width -= 1;
            this.height -= 1;
        } else {
            this.width += 1;
            this.height += 1;
        }
        ctx.fillStyle = '#000000';
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

    move() {
        this.clear();
        this.x += 10;
        this.y += 10;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

    clear(width = this.width, height = this.height) {
        ctx.clearRect(this.x, this.y, this.width, this.height);
    }

    update() {
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}
